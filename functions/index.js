/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// [START functionsimport]
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// [END functionsimport]
// [START additionalimports]
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({
  origin: true,
});
// [END additionalimports]

// [START all]
admin.initializeApp(functions.config().firebase);
// [START trigger]
exports.redirect = functions.https.onRequest((req, res) => {
  // [END trigger]
  // [START sendError]
  // Forbidding PUT requests.
  if (req.method === 'PUT') {
    return res.status(403).send('Forbidden!');
  }
  // [END sendError]
  // [START usingMiddleware]
    return admin.database().ref('urls/')
          .orderByChild('shortKey').equalTo(req.originalUrl.replace(/^.*\//, ''))
          .on('child_added', (snapshot) => {
            res.status(302).redirect(snapshot.val().title);
     });
});
// [END all]