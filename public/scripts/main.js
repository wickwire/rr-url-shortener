/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';


// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var titleInput = document.getElementById('new-url-title');
var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addUrl = document.getElementById('add-url');
var addButton = document.getElementById('add');
var recentPostsSection = document.getElementById('recent-urls-list');
var recentMenuButton = document.getElementById('menu-recent');
var listeningFirebaseRefs = [];
var urlCnt = 10001;

/*
shorten url logic sequence block
*/
var alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
var base = alphabet.length;

function findUrlInDB(urlToFind){
  var urlFound;
  var snapshot;
  firebase.database().ref('urls').orderByChild('title').equalTo(urlToFind).on('child_added', function(snapshot) {
    urlFound = snapshot.val();
  });
  return urlFound;
}

function incrementGlobalUrlCount() {
  urlCnt = urlCnt + 1;
  var urlsCntData = {
    urlsCount : urlCnt
  }
  return firebase.database().ref('/urls-counter/').update(urlsCntData);
}

function encodeToShortUrl(base10int) {
  var encoded = '';
  while (base10int){
    var remainder = base10int % base;
    base10int = Math.floor(base10int / base);
    encoded = alphabet[remainder].toString() + encoded;
  }
  return encoded;
}

function decodeFromShortUrl(base58str) {
  var decoded = 0;
  while (base58str){
    var index = alphabet.indexOf(base58str[0]);
    var power = base58str.length - 1;
    decoded += index * (Math.pow(base, power));
    base58str = base58str.substring(1);
  }
  return decoded;
}
/*
shorten url logic sequence block
*/


/**
 * Saves a new url to the Firebase DB.
 */
// [START write_fan_out]
function writeNewPost(uid, username, picture, title, body, shortKey) {
  // A url entry.
  var urlData = {
    author: username,
    uid: uid,
    intId: urlCnt,
    shortKey: shortKey,
    body: body,
    title: title,
    authorPic: picture
  };

  // Get a key for a new Post.
  var newPostKey = firebase.database().ref().child('urls').push().key;

  // Write the new url's data simultaneously in the urls list and the user's url list.
  var updates = {};
  updates['/urls/' + newPostKey] = urlData;

  return firebase.database().ref().update(updates);
}
// [END write_fan_out]

/**
 * Creates a url element.
 */
function createPostElement(urlId, title, text, author, authorId, authorPic) {
  var uid = firebase.auth().currentUser.uid;

  var html =
      '<div class="url url-' + urlId + ' mdl-cell mdl-cell--12-col ' +
                  'mdl-cell--6-col-tablet mdl-cell--4-col-desktop mdl-grid mdl-grid--no-spacing">' +
        '<div class="mdl-card mdl-shadow--2dp">' +
          '<div class="mdl-card__title mdl-color--light-blue-600 mdl-color-text--white">' +
            '<h4 class="mdl-card__title-text"></h4>' +
          '</div>' +
          '<div class="header">' +
            '<div>' +
            '</div>' +
          '</div>' +
          '<div class="text"></div>' +
        '</div>' +
      '</div>';

  // Create the DOM element from the HTML.
  var div = document.createElement('div');
  div.innerHTML = html;
  var urlElement = div.firstChild;
  // Set values.
  urlElement.getElementsByClassName('text')[0].innerText = text;
  urlElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
  return urlElement;
}

/**
 * Starts listening for new urls and populates urls lists.
 */
function startDatabaseQueries() {
  // [START my_top_urls_query]
  var myUserId = firebase.auth().currentUser.uid;
  // [END my_top_urls_query]
  // [START recent_urls_query]
  var recentPostsRef = firebase.database().ref('urls').limitToLast(100);
  // [END recent_urls_query]

  var fetchPosts = function(urlsRef, sectionElement) {
    urlsRef.on('child_added', function(data) {
      var author = data.val().author || 'Anonymous';
      var containerElement = sectionElement.getElementsByClassName('urls-container')[0];
      containerElement.insertBefore(
        createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
        containerElement.firstChild);
    });
    urlsRef.on('child_changed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('urls-container')[0];
      var urlElement = containerElement.getElementsByClassName('url-' + data.key)[0];
      urlElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
      urlElement.getElementsByClassName('username')[0].innerText = data.val().author;
      urlElement.getElementsByClassName('text')[0].innerText = data.val().body;
    });
    urlsRef.on('child_removed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('urls-container')[0];
      var url = containerElement.getElementsByClassName('url-' + data.key)[0];
      url.parentElement.removeChild(url);
    });
  };

  // Fetching and displaying all urls of each sections.
  fetchPosts(recentPostsRef, recentPostsSection);

  // Keep track of all Firebase refs we are listening to.
  listeningFirebaseRefs.push(recentPostsRef);
}

/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}
// [END basic_write]

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
  // Remove all previously displayed urls.
  recentPostsSection.getElementsByClassName('urls-container')[0].innerHTML = '';

  // Stop all currently listening Firebase listeners.
  listeningFirebaseRefs.forEach(function(ref) {
    ref.off();
  });
  listeningFirebaseRefs = [];
}

/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
var currentUID;

/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
  // We ignore token refresh events.
  if (user && currentUID === user.uid) {
    return;
  }

  cleanupUi();
  if (user) {
    currentUID = user.uid;
    splashPage.style.display = 'none';
    writeUserData(user.uid, user.displayName, user.email, user.photoURL);
    startDatabaseQueries();
  } else {
    // Set currentUID to null.
    currentUID = null;
    // Display the splash page where you can sign-in.
    splashPage.style.display = '';
  }
}

/**
 * Creates a new url for the current user.
 */
function newPostForCurrentUser(title, text) {
  // [START single_value_read]
  var shortKey;
  var userId = firebase.auth().currentUser.uid;
  var existingShortUrl;
  existingShortUrl = findUrlInDB(title);
  return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
    var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
    // [START_EXCLUDE]
    existingShortUrl = findUrlInDB(title);
    if(existingShortUrl) {
      var author = existingShortUrl.author || 'Anonymous';
        createPostElement(existingShortUrl.key, existingShortUrl.title, existingShortUrl.body, author, existingShortUrl.uid, existingShortUrl.authorPic);
    } else {
      incrementGlobalUrlCount()
        .then(function() {
        shortKey = encodeToShortUrl(urlCnt);
        text = 'https://url-shortener-26900.firebaseapp.com/' + shortKey;
        return writeNewPost(firebase.auth().currentUser.uid, username,
          firebase.auth().currentUser.photoURL,
          title, text, shortKey);
      });
    }
    // [END_EXCLUDE]
  });
  // [END single_value_read]
}

/**
 * Displays the given section element and changes styling of the given button.
 */
function showSection(sectionElement, buttonElement) {
  recentPostsSection.style.display = 'none';
  addUrl.style.display = 'none';
  recentMenuButton.classList.remove('is-active');

  if (sectionElement) {
    sectionElement.style.display = 'block';
  }
  if (buttonElement) {
    buttonElement.classList.add('is-active');
  }
}

// Bindings on load.
window.addEventListener('load', function() {
  // Bind Sign in button.
  signInButton.addEventListener('click', function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
  });

  // Bind Sign out button.
  signOutButton.addEventListener('click', function() {
    firebase.auth().signOut();
  });

  // Listen for auth state changes
  firebase.auth().onAuthStateChanged(onAuthStateChanged);

  // Saves message on form submit.
  messageForm.onsubmit = function(e) {
    e.preventDefault();
    var text = 'id';
    var title = titleInput.value;
    if (text && title) {
      newPostForCurrentUser(title, text).then(function() {
        recentMenuButton.click();
      })
      titleInput.value = '';
    }
  };

  // Bind menu buttons.
  recentMenuButton.onclick = function() {
    showSection(recentPostsSection, recentMenuButton);
  };
  addButton.onclick = function() {
    showSection(addUrl);
    titleInput.value = '';
  };
  recentMenuButton.onclick();
}, false);
